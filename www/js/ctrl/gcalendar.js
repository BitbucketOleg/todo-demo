//var apiKey = 'AIzaSyBBCLaCuJ1Be5as_6HCJY_je3EWYHAkzkc';//for browser apps
var apiKey = 'AIzaSyB8fNOg6xkdwnnU7nA0bccolyjCs-hX3OU';//for Android apps
var calendarId = 'svitla.com_a8fam310d2m64nv2gavmnu7o1s@group.calendar.google.com';
var clientId = '999388853067.apps.googleusercontent.com';
var scopes = 'https://www.googleapis.com/auth/calendar';

function getCalendarEventsList() {
	gapi.client.setApiKey(apiKey);

	gapi.client.load('calendar', 'v3', function() {
		var request = gapi.client.calendar.events.list({
			'calendarId': calendarId
		});

		request.execute(function(resp) {
			for (var i = 0; i < resp.result.items.length; i++) {
				var nodeTitle = document.createTextNode(resp.summary); //titles are undefined so I'm using the summary as title instead
				var nodeDescription = document.createTextNode(resp.description); //there are no descriptions apparently
				var nodeDate = document.createTextNode('Start: ' + resp.items[i].start.dateTime + '; End: ' + resp.items[i].end.dateTime);

				var spanTitle = document.createElement('span');
				spanTitle.appendChild(nodeTitle);
				$('#holderGoogleCalendar').append(spanTitle);

				var spanDescription = document.createElement('span');
				spanDescription.appendChild(nodeDescription);
				$('#holderGoogleCalendar').append(spanDescription);

				var spanDate = document.createElement('span');
				spanDate.appendChild(nodeDate);
				$('#holderGoogleCalendar').append(spanDate);
			}
		});
	});
};
