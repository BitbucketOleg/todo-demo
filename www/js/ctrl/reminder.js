angular.module('reminderApp', [])
	.directive('reminderDirect', function(){
		return {
			restrict: 'C',
			replace: true,
			link: function(scope, element, attrs) {
				var itm = element;
				itm.bind('mouseup', onChange);
				itm.bind('mouseout', onChange);
				function onChange() {

					if (scope.currTimestamp != this.value) {
						var arr = String(this.value).split("/");
						var dt = new Date(
							Number(arr[2]),//year
							Number(arr[0] - 1),//month
							Number(arr[1])//day
						);
						scope.currTimestamp = dt.getTime();
						//attrs.$set('showRemindersEdit', {display: ""});
						//scope.showRemindersEdit = scope.show;
					} else {
						//scope.showRemindersEdit = scope.hide;
					}
					//
				}
			}
		};
	})
	.filter('passed', function() {
		return function(val, isPassed) {
			//
			var arr = [];
			for (var i = 0; i < val.length; i++) {
				if (val[i].passed == isPassed) {
					arr.push(val[i]);
				}
			}

			return arr;
			//
		};
	})
	.controller('ReminderListCtrl', ['$scope', '$http', function($scope, $http) {
		$scope.currTimestamp = new Date().getTime();//"Please select a date";
		$scope.currReminder = "";

		$scope.hide = {display: "none"};
		$scope.show = {display: ""};
		$scope.showRemindersEdit = $scope.hide;
		$scope.showSetDate = $scope.show;
		$scope.showSetReminders = $scope.show;
		$scope.showEditReminders = $scope.hide;

		$scope.reminders = [
			{date: "09/4/2014", passed: false, itm:[
				{description: "description 0",
				passed: false,
				time: 1399582800000}]},
			{date: "10/4/2014", passed: false, itm:[
				{description: "description 1",
				passed: false,
				time: 1399669200000},
				{description: "description 2",
				passed: false,
				time: 1399669300000}]},
			{date: "11/4/2014", passed: false, itm:[
				{description: "description 2",
				passed: false,
				time: 1399755600000}]},
			{date: "12/4/2014", passed: false, itm:[
				{description: "description 3",
				passed: false,
				time: 1399842000000}]},

			{date: "01/4/2014", passed: true, itm:[
				{description: "Some done before 1",
				passed: true,
				time: 1398891600000}]},
			{date: "02/4/2014", passed: true, itm:[
				{description: "Some done before 2",
				passed: true,
				time: 1398978000000}]},
			{date: "03/4/2014", passed: true, itm:[
				{description: "Some done before 3",
				passed: true,
				time: 1399064400000}]}
		];

		$scope.onSelectCalendar = function(val) {
			$scope.showRemindersEdit = $scope.show;
			$scope.showSetReminders = $scope.show;
			$scope.showEditReminders = $scope.hide;
		};

		$scope.onSetPassed = function(val,isPassed) {
			for (var i = 0; i < $scope.reminders.length; i++) {
				//
				var isAllPassed = true;
				for (var j = 0; j < $scope.reminders[i].itm.length; j++) {
					if ($scope.reminders[i].itm[j].time == val.time) {
						$scope.reminders[i].itm[j].passed = !$scope.reminders[i].itm[j].passed;
						isPassed = $scope.reminders[i].itm[j].passed;
						//break;
					}
					if ($scope.reminders[i].itm[j].passed != isPassed) {
						isAllPassed = false;
						//break;
					}
				}
				if (isAllPassed) {
					$scope.reminders[i].passed = isPassed;
				}
				//
			}
		};

		$scope.onDelete = function(val) {
			//
			for (var i = 0; i < $scope.reminders.length; i++) {
				for (var j = 0; j < $scope.reminders[i].itm.length; j++) {
					//
					if (val.hasOwnProperty("itm")) {

						for (var k = 0; k < val.itm.length; k++) {
							if ($scope.reminders[i].itm[j].time == val.itm[k].time) {
								$scope.reminders[i].itm.splice(j,1);
								//break;
							}
						}

					} else {
						if ($scope.reminders[i].itm[j].time == val.time) {
							$scope.reminders[i].itm.splice(j,1);
							//break;
						}
					}
					//
				}
			}
			//
			for (var i = 0; i < $scope.reminders.length; i++) {
				if ($scope.reminders[i].itm.length == 0) {
					$scope.reminders.splice(i,1);
					//break;
				}
			}
			//
		};

		$scope.currEditID = null;
		$scope.currEditSubID = null;
		$scope.onEditReminderTxt = function(val) {
			$scope.onDelete($scope.reminders[$scope.currEditID].itm[$scope.currEditSubID]);

			$scope.onSetReminderTxt(val);
		};
		$scope.onEdit = function(val) {
			$scope.showRemindersEdit = $scope.show;
			$scope.showSetDate = $scope.hide;
			$scope.showSetReminders = $scope.hide;
			$scope.showEditReminders = $scope.show;
			//
			for (var i = 0; i < $scope.reminders.length; i++) {
				for (var j = 0; j < $scope.reminders[i].itm.length; j++) {
					//
					if (val.hasOwnProperty("itm")) {

						for (var k = 0; k < val.itm.length; k++) {
							if ($scope.reminders[i].itm[j].time == val.itm[k].time) {
								$scope.currReminder = $scope.reminders[i].itm[j].description;
								$scope.currEditID = i;
								$scope.currEditSubID = j;
							}
						}

					} else {
						if ($scope.reminders[i].itm[j].time == val.time) {
							$scope.currReminder = $scope.reminders[i].itm[j].description;
							$scope.currEditID = i;
							$scope.currEditSubID = j;
						}
					}
					//
				}
			}
			//
		};

		$scope.onSetReminderTxt = function(val) {
			var dt = new Date($scope.currTimestamp);

			var d = new Date();
			var today = new Date(d.getFullYear(),d.getMonth(),d.getDate());
			today = today.getTime();

			var sDay = dt.getDate();
			var sMonth = (dt.getMonth() + 1);
			var sYear = dt.getFullYear();
			var sFullDate = sDay+"/"+sMonth+"/"+sYear;

			var isEx = null;
			var obj = {description: val,
				passed: ($scope.currTimestamp < today),
				time: $scope.currTimestamp};

			for (var i = 0; i < $scope.reminders.length; i++) {
				if ($scope.reminders[i].date == sFullDate) {
					isEx = i;
					break;
				}
			}

			if (isEx == null) {
				$scope.reminders.push({date: sFullDate,
					passed: ($scope.currTimestamp < today),
					itm:[obj]});
			} else {
				$scope.reminders[isEx].itm.passed = ($scope.currTimestamp < today);
				$scope.reminders[isEx].itm.push(obj);
			}

			$scope.showRemindersEdit = $scope.hide;
		};
	}]);